# NumpadArtist
This is some little bashscript to change your usb numpad become a digital artist shortcut. 
```
sudo apt install x11-utils x11-xserver-utils
git clone https://gitlab.com/sira313/numpad-artist
```

Give **numpad-artist** permission to excute as program and run every you want to draw with krita. 

Move `ShortcutforKrita.shortcuts` to `~/.local/share/krita/input`

This script tested with krita on Ubuntu 16.04LTS and KDE Neon (2019). **If it not working on your device**, you should edit **numpad-artist** file following your `keycode` and `keysymname` using `xev` command.
For something else, you should configure krita shortcut on **settings menu** and create your own shortcut as you want.

![](https://4.bp.blogspot.com/-HJQXX2pXmsg/W0U-2YFE_4I/AAAAAAAABPQ/WR2TaIouSfkiL2855ODks-13FfvnNUKDgCLcBGAs/s1600/bitmap.png)
